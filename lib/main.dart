import 'package:easy_localization/easy_localization.dart';
import 'package:faster/components/game_status_component.dart';
import 'package:faster/faster_game.dart';
import 'package:faster/layers/faster_credits_layer.dart';
import 'package:faster/layers/faster_dead_layer.dart';
import 'package:faster/layers/faster_home_layer.dart';
import 'package:faster/layers/faster_paused_layer.dart';
import 'package:faster/layers/faster_playing_layer.dart';
import 'package:faster/layers/faster_scores_layer.dart';
import 'package:faster/utils/database/high_scores.dart';
import 'package:faster/utils/di.dart';
import 'package:flame/game.dart';
import 'package:flame_audio/flame_audio.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await getDependencies();

  runApp(EasyLocalization(
    supportedLocales: const [Locale('en', 'US'), Locale('fr', 'FR')],
    path: 'assets/i18n',
    fallbackLocale: const Locale('en', 'US'),
    useOnlyLangCode: true,
    child: const App(),
  ));
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  FasterGame? _game;

  bool _showHighScores = false;

  @override
  void initState() {
    super.initState();
    _game = FasterGame(setCurrentScore: _setCurrentScore);
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState appState) {
    GameStatus? gameStatus = _game!.getGameStatus();
    if (gameStatus != null) {
      switch (appState) {
        case AppLifecycleState.resumed:

          /// do nothing
          break;
        case AppLifecycleState.inactive:
        case AppLifecycleState.paused:
        case AppLifecycleState.detached:
          if (gameStatus == GameStatus.playing) _game!.setPaused();
          break;
      }
    }
  }

  void _setCurrentScore(int score) => getIt<HighScores>().currentScore = score;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'ActayWide'),
      home: Scaffold(
        body: Stack(
          children: [
            Sizer(builder: (BuildContext context, Orientation orientation, DeviceType deviceType) => const SizedBox()),
            GameWidget(
              game: _game!,
              overlayBuilderMap: {
                FasterHome.name: (BuildContext context, FasterGame game) {
                  return FasterHome(() {
                    game.setCredits();
                  }, () {
                    FlameAudio.play('click.mp3');
                    game.setPlaying();
                  }, () {
                    game.setScores();
                  }, () {
                    _showHighScores = !_showHighScores;
                    if (_showHighScores) {
                      game.setScores();
                    } else {
                      game.setCredits();
                    }
                  });
                },
                FasterCredits.name: (BuildContext context, FasterGame game) {
                  return FasterCredits(() {
                    game.setHome();
                  }, () {
                    game.setHome();
                  });
                },
                FasterPlaying.name: (BuildContext context, FasterGame game) {
                  return FasterPlaying(() {
                    FlameAudio.play('click.mp3');
                    game.setPaused();
                  });
                },
                FasterPaused.name: (BuildContext context, FasterGame game) {
                  return FasterPaused(() {
                    FlameAudio.play('click.mp3');
                    game.unsetPause();
                  });
                },
                FasterDead.name: (BuildContext context, FasterGame game) {
                  return FasterDead(() {
                    FlameAudio.play('click.mp3');
                    game.setPlaying();
                  }, () {
                    game.setHome();
                  });
                },
                FasterScores.name: (BuildContext context, FasterGame game) {
                  return FasterScores(() {
                    game.setHome();
                  }, () {
                    game.setHome();
                  });
                },
              },
              initialActiveOverlays: [
                FasterHome.name,
              ],
            ),
          ],
        ),
      ),
    );
  }
}
