import 'package:audioplayers/audioplayers.dart';
import 'package:faster/components/animated_sprites_component.dart';
import 'package:faster/components/difficulty_component.dart';
import 'package:faster/components/game_status_component.dart';
import 'package:faster/components/hitbox_component.dart';
import 'package:faster/components/parallax_component.dart';
import 'package:faster/components/score_component.dart';
import 'package:faster/components/tap_input_component.dart';
import 'package:faster/components/velocity_component.dart';
import 'package:faster/constants/velocity.dart';
import 'package:faster/entities/game_session_entity.dart';
import 'package:faster/entities/obstacle_entity.dart';
import 'package:faster/entities/player_entity.dart';
import 'package:faster/systems/animated_sprite_system.dart';
import 'package:faster/systems/background_system.dart';
import 'package:faster/systems/collision_system.dart';
import 'package:faster/systems/difficulty_system.dart';
import 'package:faster/systems/game_status_system.dart';
import 'package:faster/systems/hitbox_system.dart';
import 'package:faster/systems/jump_system.dart';
import 'package:faster/systems/obstacle_system.dart';
import 'package:faster/systems/particle_disposal_system.dart';
import 'package:faster/systems/score_system.dart';
import 'package:faster/systems/sprite_system.dart';
import 'package:faster/utils/parallax_backgrounds/parallax_backgrounds.dart';
import 'package:faster/utils/patterns/obstacle_patterns.dart';
import 'package:flame/flame.dart';
import 'package:flame/input.dart';
import 'package:flame/parallax.dart';
import 'package:flame_audio/flame_audio.dart';
import 'package:flame_oxygen/flame_oxygen.dart';

class FasterGame extends OxygenGame with TapDetector {
  final SetIntCallback setCurrentScore;

  AudioPlayer? _player;

  FasterGame({required this.setCurrentScore});

  @override
  Future<void> init() async {
    await Flame.device.fullScreen();
    await Flame.device.setLandscape();

    final patternsList = await loadPatterns();
    FlameAudio.bgm.initialize();

    FlameAudio.audioCache.load('music.mp3');

    await FlameAudio.audioCache.loadAll(['click.mp3', 'death.mp3', 'jump.wav']);

    var parallaxBackgrounds = await ParallaxBackgrounds.load(baseVelocity);

    world
      ..registerSystem(BackgroundSystem(parallaxBackgrounds))
      ..registerSystem(SpriteSystem())
      ..registerSystem(GameStatusSystem())
      ..registerSystem(AnimatedSpriteSystem())
      ..registerSystem(JumpSystem())
      ..registerSystem(DifficultySystem())
      ..registerSystem(ObstacleSystem(patternsList))
      ..registerSystem(HitBoxSystem())
      ..registerSystem(CollisionSystem())
      ..registerSystem(ScoreSystem(setCurrentScore: setCurrentScore))
      ..registerSystem(ParticleSystem())
      ..registerSystem(ParticleDisposalSystem())
      ..registerComponent<ParticleComponent, Particle>(() => ParticleComponent())
      ..registerComponent<VelocityComponent, Vector2>(() => VelocityComponent())
      ..registerComponent<TapInputComponent, bool>(() => TapInputComponent())
      ..registerComponent<ParallaxComponent, Parallax>(() => ParallaxComponent())
      ..registerComponent<DifficultyComponent, double>(() => DifficultyComponent())
      ..registerComponent<GameStatusComponent, GameStatus>(() => GameStatusComponent())
      ..registerComponent<AnimatedSpritesComponent, List<SpriteAnimation>>(() => AnimatedSpritesComponent())
      ..registerComponent<HitBoxComponent, void>(() => HitBoxComponent())
      ..registerComponent<ScoreComponent, double>(() => ScoreComponent());

    createGameSession(this);
    await createPlayer(this);
  }

  @override
  void onTapCancel() => _revertIsTapped();

  @override
  void onTapDown(TapDownInfo info) {
    FlameAudio.play('jump.wav', volume: 0.8).then((player) => _player = player);
    _revertIsTapped();
  }

  @override
  void onTapUp(TapUpInfo info) => _revertIsTapped();

  void _revertIsTapped() {
    if (isPlaying()) {
      final player = world.entityManager.getEntityByName(playerEntity);

      player?.get<TapInputComponent>()!.revert();
    }
    _player?.stop();
  }

  GameStatus? getGameStatus() =>
      world.entityManager.getEntityByName(gameSessionEntity)?.get<GameStatusComponent>()?.status;

  bool isPlaying() =>
      world.entityManager.getEntityByName(gameSessionEntity)?.get<GameStatusComponent>()?.status == GameStatus.playing;

  setPlaying() {
    Entity? gameEntity = world.entityManager.getEntityByName(gameSessionEntity);
    if (gameEntity != null) {
      for (var entity in world.entities) {
        if (entity.name?.startsWith(obstacleEntity) ?? false) {
          world.entityManager.removeEntity(entity);
        }
      }

      gameEntity.get<DifficultyComponent>()?.difficulty = 1;
      gameEntity.get<GameStatusComponent>()?.status = GameStatus.playing;

      world.entityManager.processRemovedEntities();
    }
    Entity? entity = world.entityManager.getEntityByName(playerEntity);
    if (entity != null) {
      entity.get<VelocityComponent>()!.reset();
      entity.get<PositionComponent>()!.position.setFrom(Vector2(50, world.game.size.y));
      entity.get<TapInputComponent>()!.value = false;
    }
  }

  setPaused() {
    Entity? gameEntity = world.entityManager.getEntityByName(gameSessionEntity);
    if (gameEntity != null) {
      gameEntity.get<GameStatusComponent>()?.status = GameStatus.paused;
    }
  }

  setCredits() {
    Entity? gameEntity = world.entityManager.getEntityByName(gameSessionEntity);
    if (gameEntity != null) {
      gameEntity.get<GameStatusComponent>()?.status = GameStatus.credits;
    }
  }

  setScores() {
    Entity? gameEntity = world.entityManager.getEntityByName(gameSessionEntity);
    if (gameEntity != null) {
      gameEntity.get<GameStatusComponent>()?.status = GameStatus.scores;
    }
  }

  setHome() {
    // Entity? gameEntity = world.entityManager.getEntityByName(gameSessionEntity);
    // if (gameEntity != null) {
    //   gameEntity.get<GameStatusComponent>()?.status = GameStatus.home;
    // }
    Entity? gameEntity = world.entityManager.getEntityByName(gameSessionEntity);
    if (gameEntity != null) {
      for (var entity in world.entities) {
        if (entity.name?.startsWith(obstacleEntity) ?? false) {
          world.entityManager.removeEntity(entity);
        }
      }

      world.entityManager.processRemovedEntities();

      gameEntity.get<GameStatusComponent>()?.status = GameStatus.home;
    }
    Entity? entity = world.entityManager.getEntityByName(playerEntity);
    if (entity != null) {
      entity.get<VelocityComponent>()!.reset();
      entity.get<PositionComponent>()!.position.setFrom(Vector2(50, world.game.size.y));
      entity.get<TapInputComponent>()!.value = false;
    }
  }

  unsetPause() {
    Entity? gameEntity = world.entityManager.getEntityByName(gameSessionEntity);
    if (gameEntity != null) {
      gameEntity.get<GameStatusComponent>()?.status = GameStatus.playing;
    }
  }
}
