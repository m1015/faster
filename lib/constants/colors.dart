import 'dart:ui';

abstract class FasterColors {
  static Color white = const Color(0xffFFFFFF);
  static Color orange = const Color(0xFFFC5E04);
  static Color lightOrange = const Color(0xFFF87C30);
  static Color darkOrange = const Color(0xFFEF440A);
  static Color brown = const Color(0xFF7A6563);
}
