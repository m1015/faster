import 'package:sizer/sizer.dart';

abstract class FasterSizes {
  static double xxsmall = 2;
  static double xsmall = 4;
  static double small = 8;
  static double medium = 16;
  static double large = 32;

  static double hugeMedium = 16.h;
  static double hugeLarge = 20.h;
}

abstract class FasterRadius {
  static double regular = 16;
}