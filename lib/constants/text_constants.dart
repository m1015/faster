import 'package:faster/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

final textTheme = TextTheme(
  headlineLarge: TextStyle(
    fontSize: 48.sp,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w600,
    color: FasterColors.white,
  ),
  headlineMedium: TextStyle(
    fontSize: 36.sp,
    letterSpacing: -0.2,
    fontWeight: FontWeight.w500,
    color: FasterColors.white,
  ),
  headlineSmall: TextStyle(
    fontSize: 32.sp,
    letterSpacing: -0.2,
    fontWeight: FontWeight.w500,
    color: FasterColors.white,
  ),
  titleLarge: TextStyle(
    fontSize: 22.sp,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w400,
    color: FasterColors.white,
  ),
  titleMedium: TextStyle(
    fontSize: 18.sp,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w400,
    color: FasterColors.white,
  ),
  titleSmall: TextStyle(
    fontSize: 12.sp,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w400,
    color: FasterColors.white,
  ),
);
