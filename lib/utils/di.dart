import 'package:faster/utils/database/high_scores.dart';
import 'package:get_it/get_it.dart';

GetIt getIt = GetIt.instance;

Future<void> getDependencies() async {
  getIt.registerSingletonAsync<HighScores>(() async {
    final highScores = HighScores();
    await highScores.init();
    return highScores;
  });
}