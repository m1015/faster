import 'package:faster/utils/database/models/user_score.dart';
import 'package:objectdb/objectdb.dart';
import 'package:objectdb/src/objectdb_storage_filesystem.dart';
import 'package:path_provider/path_provider.dart';

class LocalRepository {
  static LocalRepository? _instance;

  static Future<LocalRepository> getInstance() async {
    if (_instance == null) {
      _instance = LocalRepository._();
      await _instance!.init();
    }
    return _instance!;
  }

  late ObjectDB _db;

  LocalRepository._();

  Future<void> init() async {
    final path = '${(await getApplicationDocumentsDirectory()).path}/faster.db';
    _db = ObjectDB(FileSystemStorage(path));
  }

  Future<PlayerScore?> getHighScoreForPlayer(String player) async {
    final result = await _db.find({'user': player});

    // TODO : prendre le meilleur ?

    if (result.isEmpty) return null;

    return PlayerScore.fromJson(result.first);
  }

  Future<void> saveScoreForPlayer(PlayerScore playerScore) async {
    final previous = await getHighScoreForPlayer(playerScore.player);

    if (previous == null) {
      await _db.insert(playerScore.toJson());
    } else {
      await _db.update(
        {'user': playerScore.player},
        {'score': playerScore.score, 'date': playerScore.date.toIso8601String()},
      );
    }
  }

  Future<List<PlayerScore>> getHighScores() async {
    final data = await _db.find();

    final scores = data.map((d) => PlayerScore.fromJson(d)).toList();

    scores.sort((a, b) {
      final scoreDiff = b.score - a.score;
      if (scoreDiff == 0) {
        return a.date.compareTo(b.date);
      }
      return scoreDiff;
    });

    return scores.take(10).toList();
  }
}
