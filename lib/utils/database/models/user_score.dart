class PlayerScore {
  late String player;

  late int score;

  late DateTime date;

  PlayerScore({required this.player, required this.score, required this.date});

  factory PlayerScore.fromJson(Map<dynamic, dynamic> json) => PlayerScore(
        player: json['user'],
        score: json['score'],
        date: DateTime.parse(json['date']),
      );

  Map<dynamic, dynamic> toJson() => {
        'user': player,
        'score': score,
        'date': date.toIso8601String(),
      };
}
