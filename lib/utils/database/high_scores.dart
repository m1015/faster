import 'package:faster/utils/database/local_repository.dart';
import 'package:faster/utils/database/models/user_score.dart';

class HighScores {
  int? _currentScore;

  late LocalRepository repository;

  Future<void> init() async => repository = await LocalRepository.getInstance();

  set currentScore(int score) => _currentScore = score;

  int get currentScore => _currentScore ?? 0;

  Future<PlayerScore?> loadHighScoreForPlayer(String player) => repository.getHighScoreForPlayer(player);

  Future<void> saveHighScore(String player, int score) =>
      repository.saveScoreForPlayer(PlayerScore(player: player, score: score, date: DateTime.now()));

  Future<List<PlayerScore>> loadAllHighScores() => repository.getHighScores();
}
