import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:faster/constants/colors.dart';
import 'package:faster/constants/sizes.dart';
import 'package:faster/constants/text_constants.dart';
import 'package:faster/layers/widgets/full_screen_card_widget.dart';
import 'package:faster/utils/database/high_scores.dart';
import 'package:faster/utils/database/models/user_score.dart';
import 'package:faster/utils/di.dart';
import 'package:flutter/material.dart';

class FasterScores extends StatefulWidget {
  static String name = "faster_scores";

  final VoidCallback onPressed;

  final VoidCallback onCountdownReached;

  const FasterScores(this.onPressed, this.onCountdownReached, {Key? key}) : super(key: key);

  @override
  State<FasterScores> createState() => _FasterScoresState();
}

class _FasterScoresState extends State<FasterScores> {
  Timer? _countDown;

  List<PlayerScore>? _scores;

  final TextStyle textStyle = textTheme.titleMedium!;
  final TextStyle textStyleBold = textTheme.titleMedium!.copyWith(fontWeight: FontWeight.bold);

  @override
  void initState() {
    super.initState();

    final highScores = getIt<HighScores>();

    highScores.loadAllHighScores().then((value) => setState(() => _scores = value));

    _countDown = Timer(const Duration(seconds: 15), () => widget.onCountdownReached());
  }

  @override
  void dispose() {
    _countDown?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var index = 1;

    return FullScreenCard(
      onPressed: () {
        _countDown?.cancel();
        widget.onPressed();
      },
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(FasterSizes.large),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                tr('labels.highScoresTitle').toUpperCase(),
                style: textTheme.titleMedium!.copyWith(color: FasterColors.orange),
              ),
              SizedBox(height: FasterSizes.small),
              Row(
                children: [
                  Expanded(flex: 1, child: Text(tr('labels.highScoresRank').toUpperCase(), style: textStyleBold)),
                  Expanded(flex: 3, child: Text(tr('labels.highScoresPlayer').toUpperCase(), style: textStyleBold)),
                  Expanded(flex: 2, child: Text(tr('labels.highScoresScore').toUpperCase(), style: textStyleBold)),
                ],
              ),
              if (_scores != null)
                ..._scores!.map((PlayerScore score) {
                  return Row(
                    children: [
                      Expanded(flex: 1, child: Text((index++).toString(), style: textStyle)),
                      Expanded(flex: 3, child: Text(score.player, style: textStyle)),
                      Expanded(flex: 2, child: Text(score.score.toString(), style: textStyle)),
                    ],
                  );
                }),
            ],
          ),
        ),
      ),
    );
  }
}
