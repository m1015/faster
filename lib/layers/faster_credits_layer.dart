import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:faster/constants/sizes.dart';
import 'package:faster/constants/text_constants.dart';
import 'package:faster/layers/widgets/full_screen_card_widget.dart';
import 'package:flutter/material.dart';

class FasterCredits extends StatefulWidget {
  static String name = "faster_credits";

  final VoidCallback onPressed;

  final VoidCallback onCountdownReached;

  const FasterCredits(this.onPressed, this.onCountdownReached, {Key? key}) : super(key: key);

  @override
  State<FasterCredits> createState() => _FasterCreditsState();
}

class _FasterCreditsState extends State<FasterCredits> {
  Timer? _countDown;

  @override
  void initState() {
    _countDown = Timer(const Duration(seconds: 15), () => widget.onCountdownReached());
    super.initState();
  }

  @override
  void dispose() {
    _countDown?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FullScreenCard(
      onPressed: () {
        _countDown?.cancel();
        widget.onPressed();
      },
      child: Padding(
        padding: EdgeInsets.all(FasterSizes.medium),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(tr('labels.thanks').toUpperCase(), style: textTheme.titleMedium),
                Text(tr('labels.guillaume'), style: textTheme.titleMedium),
                Text(tr('labels.simon'), style: textTheme.titleMedium),
                Text(tr('labels.samuel'), style: textTheme.titleMedium),
                Text(tr('labels.maxime'), style: textTheme.titleMedium),
                Text(tr('labels.lamine'), style: textTheme.titleMedium),
                Text(tr('labels.mathilde'), style: textTheme.titleMedium),
                SizedBox(height: FasterSizes.small),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(tr('labels.made').toUpperCase(), style: textTheme.titleMedium),
                const Spacer(),
                Image.asset('assets/images/flame_img.png', height: FasterSizes.hugeMedium),
                const Spacer(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
