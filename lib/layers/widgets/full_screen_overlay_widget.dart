import 'package:faster/constants/colors.dart';
import 'package:faster/constants/sizes.dart';
import 'package:flutter/material.dart';

class FullScreenOverlay extends StatelessWidget {
  final Widget child;

  final VoidCallback onPressed;

  const FullScreenOverlay({Key? key, required this.child, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onPressed,
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child:  Padding(
          padding: EdgeInsets.all(FasterSizes.large),
          child: child,
        ),
      ),
    );
  }
}
