import 'package:faster/constants/colors.dart';
import 'package:faster/constants/sizes.dart';
import 'package:flutter/material.dart';

class FullScreenCard extends StatelessWidget {
  final Widget child;

  final VoidCallback onPressed;

  const FullScreenCard({Key? key, required this.child, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onPressed,
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: EdgeInsets.all(FasterSizes.large),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.5),
              borderRadius: BorderRadius.circular(FasterRadius.regular),
              border: Border.all(
                color: FasterColors.orange,
                width: FasterSizes.xxsmall,
              ),
            ),
            child: child,
          ),
        ),
      ),
    );
  }
}
