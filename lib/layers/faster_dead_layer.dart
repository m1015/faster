import 'dart:async';
import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:faster/constants/colors.dart';
import 'package:faster/constants/sizes.dart';
import 'package:faster/constants/text_constants.dart';
import 'package:faster/layers/widgets/full_screen_overlay_widget.dart';
import 'package:faster/utils/database/high_scores.dart';
import 'package:faster/utils/di.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:sizer/sizer.dart';

class FasterDead extends StatefulWidget {
  static String name = 'faster_dead';

  final VoidCallback onPressed;

  final VoidCallback onCountdownReached;

  const FasterDead(this.onPressed, this.onCountdownReached, {Key? key}) : super(key: key);

  @override
  State<FasterDead> createState() => _FasterDeadState();
}

class _FasterDeadState extends State<FasterDead> {
  final TextEditingController controller = TextEditingController();

  int? _playerHighScore;

  String _playerName = '';

  Timer? _countDown;

  late HighScores _highScores;

  late int _currentScore;

  bool _done = false;

  @override
  void initState() {
    super.initState();

    _highScores = getIt<HighScores>();

    _currentScore = getIt<HighScores>().currentScore;
  }

  @override
  void dispose() {
    controller.dispose();
    _countDown?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FullScreenOverlay(
      onPressed: _onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset('assets/images/dead.png', width: 80.h),
          if (!_done)
            PinCodeTextField(
              autofocus: true,
              controller: controller,
              hideCharacter: false,
              highlight: true,
              highlightColor: FasterColors.orange,
              defaultBorderColor: FasterColors.orange,
              hasTextBorderColor: FasterColors.orange,
              maxLength: 3,
              keyboardType: TextInputType.text,
              pinTextStyle: textTheme.titleMedium!.copyWith(color: FasterColors.white),
              pinBoxColor: Colors.transparent,
              pinBoxWidth: 90,
              pinBoxHeight: 90,
              pinBoxRadius: FasterSizes.small,
              onTextChanged: (String text) {
                final updatedText = controller.text.toUpperCase();
                controller.value = controller.value.copyWith(
                  text: updatedText,
                  selection: TextSelection.collapsed(offset: updatedText.length),
                );
              },
              onDone: (String player) async {
                _playerName = player;
                int score = (await _highScores.loadHighScoreForPlayer(player))?.score ?? 0;
                setState(() {
                  _done = true;
                  _playerHighScore = max(score, _currentScore);
                  FocusManager.instance.primaryFocus?.unfocus();
                });
                if (_currentScore > score) await _highScores.saveHighScore(_playerName, _currentScore);
                _countDown = Timer(const Duration(seconds: 15), () => widget.onCountdownReached());
              },
            ),
          Text(
            '${tr('labels.currentScore')}: $_currentScore',
            style: textTheme.titleLarge!.copyWith(color: FasterColors.lightOrange),
          ),
          if (_playerHighScore != null)
            Text(
              '${tr('labels.highScore')}: $_playerHighScore',
              style: textTheme.titleSmall!.copyWith(color: FasterColors.darkOrange),
            ),
          if (_playerName.isNotEmpty)
            Container(
              width: FasterSizes.hugeMedium,
              decoration: const BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/images/button.png'), fit: BoxFit.fill),
              ),
              child: Center(
                child: Text(
                  tr('actions.retry'),
                  style: textTheme.titleSmall,
                ),
              ),
            ),
        ],
      ),
    );
  }

  void _onTap() async {
    if (_playerName.isNotEmpty) {
      _countDown?.cancel();
      widget.onPressed();
    }
  }
}
