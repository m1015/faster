import 'dart:async';

import 'package:faster/constants/sizes.dart';
import 'package:faster/constants/text_constants.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class FasterPaused extends StatefulWidget {
  static String name = 'faster_paused';

  final VoidCallback onPressed;

  const FasterPaused(this.onPressed, {Key? key}) : super(key: key);

  @override
  State<FasterPaused> createState() => _FasterPausedState();
}

class _FasterPausedState extends State<FasterPaused> {
  Timer? _timer;
  int _ticker = 3;
  bool _restarting = false;

  void _startTicker() {
    setState(() {
      _restarting = true;
    });
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        --_ticker;
      });
      if (_ticker < 1) widget.onPressed();
    });
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          _startTicker();
        },
        child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: _restarting
                ? Center(
                    child: Text(
                    _ticker.toString(),
                    style: textTheme.headlineLarge,
                  ))
                : const Center(child: _Paused())));
  }
}

class _Paused extends StatelessWidget {
  const _Paused({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 20.h,
        // height: FasterSizes.hugeMedium,
        decoration: const BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/images/paused.png'), fit: BoxFit.contain),
        ));
  }
}
