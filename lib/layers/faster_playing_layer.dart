import 'package:faster/constants/colors.dart';
import 'package:faster/constants/sizes.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class FasterPlaying extends StatelessWidget {
  static String name = 'faster_playing';

  final VoidCallback onPaused;

  const FasterPlaying(this.onPaused, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned(
        top: 0,
        right: 0,
        child: IconButton(
          iconSize: FasterSizes.xsmall.h,
          icon: ImageIcon(
            const AssetImage("assets/images/pause.png"),
            color: FasterColors.white,
            size: FasterSizes.xsmall.h,
          ),
          onPressed: () => onPaused(),
        ),
      )
    ]);
  }
}
