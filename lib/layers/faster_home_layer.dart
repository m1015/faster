import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:faster/constants/colors.dart';
import 'package:faster/constants/sizes.dart';
import 'package:faster/constants/text_constants.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class FasterHome extends StatefulWidget {
  static String name = 'faster_home';

  final VoidCallback onCreditsPressed;

  final VoidCallback onPlayPressed;

  final VoidCallback onScoresPressed;

  final VoidCallback onCountdownReached;

  const FasterHome(this.onCreditsPressed, this.onPlayPressed, this.onScoresPressed, this.onCountdownReached, {Key? key}) : super(key: key);

  @override
  State<FasterHome> createState() => _FasterHomeState();
}

class _FasterHomeState extends State<FasterHome> {
  Timer? _countDown;

  @override
  void initState() {
    _countDown = Timer(const Duration(seconds: 15), () => widget.onCountdownReached());
    super.initState();
  }

  @override
  void dispose() {
    _countDown?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Flexible(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  iconSize: FasterSizes.xsmall.h,
                  icon: ImageIcon(
                    const AssetImage("assets/icons/scores.png"),
                    color: FasterColors.white,
                    size: FasterSizes.xsmall.h,
                  ),
                  onPressed: () => _onPressed(widget.onScoresPressed),
                ),
                IconButton(
                  iconSize: FasterSizes.xsmall.h,
                  icon: ImageIcon(
                    const AssetImage("assets/icons/credits.png"),
                    color: FasterColors.white,
                    size: FasterSizes.xsmall.h,
                  ),
                  onPressed: () => _onPressed(widget.onCreditsPressed),
                )
              ],
            ),
          ),
          SizedBox(height: 10.h),
          Image.asset('assets/images/faster.png', width: 80.h),
          SizedBox(height: 10.h),
          // const Spacer(),
          GestureDetector(
            onTap: () => _onPressed(widget.onPlayPressed),
            child: Container(
              width: FasterSizes.hugeMedium,
              decoration: const BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/images/button.png'), fit: BoxFit.fill),
              ),
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: FasterSizes.small, horizontal: FasterSizes.medium),
                  child: FittedBox(
                    child: Text(
                      tr('actions.play'),
                      style: textTheme.titleLarge!.copyWith(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onPressed(VoidCallback callback) {
    _countDown?.cancel();
    callback();
  }
}
