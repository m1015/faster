import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:faster/components/hitbox_component.dart';
import 'package:faster/components/velocity_component.dart';
import 'package:faster/faster_game.dart';
import 'package:flame/game.dart';
import 'package:flame_oxygen/flame_oxygen.dart';

const obstacleEntity = 'Obstacle';

const baseObstacleVelocity = 200.0;
const obstacleSizeFactors = [5.5, 6, 6.5];

Future<Entity> createObstacle(
  int number,
  int obstacleSizeIndex,
  FasterGame game,
  {
    required double positionX,
    required double positionY,
    required double deltaX,
    required double deltaY,
  }
) async {
  Random random = Random();

  final yVariation = (random.nextDouble() * 5 - 2.5) * deltaY;
  final obstacleSize = game.size.y / obstacleSizeFactors[obstacleSizeIndex];

  double obstacleVelocity = -baseObstacleVelocity + (random.nextDouble() * 30 - 15);

  return game.createEntity(
    name: '$obstacleEntity$number',
    position: Vector2(
      (game.size.x + positionX * game.size.x),
      (positionY * (game.size.y - obstacleSize)) * (1 + yVariation),
    ),
    size: Vector2.all(
      obstacleSize,
    ),
  )
    ..add<SpriteComponent, SpriteInit>(
        SpriteInit(await game.loadSprite('brickSpecial${NumberFormat('00').format(random.nextInt(9) + 1)}.png')))
    ..add<VelocityComponent, Vector2>(
      Vector2(obstacleVelocity, 0),
    )
    ..add<HitBoxComponent, void>();
}
